from datetime import datetime

# Date de début de tâche
start = datetime.now()

import os
import pandas as pd 


# Si existant, récupérer le fichier de logs précédent
if os.path.exists("/opt/airflow/dags/exemple/ex_logs/ex_logs_TC.csv") :

	logs = pd.read_csv("/opt/airflow/dags/exemple/ex_logs/ex_logs_TC.csv", sep = ";", decimal = ',')

# Sinon créer un dataframe vide avec les variables souhiatées
else :
	logs = pd.DataFrame({
		"task_id" : [],
		"start" : [],
		"end" :[],
		"status" : [],
	})


# Tâche principale
print("Done")

# Date de fin de tâche
end = datetime.now()

# Logs de l'exécution
new_logs = pd.DataFrame({
	"task_id" : ["execute_python"],
	"start" : [start],
	"end" : [end],
	"status" : ["succes"]})


# Compil avec les précédents logs
all_logs = pd.concat([logs, new_logs])


# Exports
all_logs.to_csv("/opt/airflow/dags/exemple/ex_logs/ex_logs_TC.csv",
	sep = ';' , decimal = ',', index = False)