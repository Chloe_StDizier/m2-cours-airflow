from airflow import DAG
from airflow.operators.bash import BashOperator

from datetime import datetime, timedelta































with DAG(
    'exemple1',
    description='Mon premier DAG',
    schedule_interval = timedelta(days = 30),
    start_date=datetime(2024, 5, 9),
    catchup = False

) as dag:

    


























    
    t1 = BashOperator(
        task_id='lister_contenu',
        bash_command='ls ')


    t2 = BashOperator(
        task_id='execute_python',
        bash_command="python3 /opt/airflow/dags/exemple/scripts_autres/script_exemple.py")

    t3 = BashOperator(
        task_id='execute_python_2',
        bash_command="python3 /opt/airflow/dags/exemple/scripts_autres/script_exemple.py")
















    t1 >>  [t2 , t3]