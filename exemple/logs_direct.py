from airflow import DAG
from airflow.operators.bash import BashOperator

from datetime import datetime, timedelta


with DAG(
    'exemple_logs_direct',
    description='Exemple de récupération des logs directement au sein de la tâche',
    schedule_interval = None,
    start_date=datetime(2024, 5, 9),
    catchup = False,
    is_paused_upon_creation = True

) as dag:

    
    t1 = BashOperator(
        task_id='lister_contenu',
        bash_command='ls /opt/airflow')


    t2 = BashOperator(
        task_id='execute_python',
        bash_command="python3 /opt/airflow/dags/scripts_autres/script_logs_direct.py")



    t1 >>  t2 